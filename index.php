<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Список");
?>
<ul>
    <li><a href="ajax/">ajax</a></li>
    <li><a href="form/">form</a></li>
    <li><a href="form.result.new/">form.result.new</a></li>
    <li><a href="test.form/">test.form</a></li>
    <li><a href="news/">news</a></li>
    <li><a href="news.filter//">news.filter</a></li>
    <li><a href="news.list/">news.list</a></li>
    <li><a href="news.list.filter/">news.list.filter</a></li>
    <li><a href="test.list/">test.list</a></li>
    <li><a href="user.enter/">user.enter</a></li>
    <li><a href="user.register/">user.register</a></li>
    <li><a href="user.forgotpasswd/">user.forgotpasswd</a></li>
</ul>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>