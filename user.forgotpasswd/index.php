<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("wrapper.test.list");
?>
<?php $APPLICATION->IncludeComponent(
	"project.ajax:wrapper",
	"wrapper.test.list",
	[
		'IS_RELOAD_PAGE' => 'Y',
		'PAGEN' => Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('PAGEN_1'),
		'PARAMS' => [
		],
	],
	$component
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>