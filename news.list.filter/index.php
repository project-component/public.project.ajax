<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("news.list");
?>
<?php $APPLICATION->IncludeComponent(
	"project.ajax:wrapper",
	"news.list.filter",
	[
		'IS_RELOAD_PAGE' => 'Y',
		'PAGEN' => Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('PAGEN_1'),
		'PARAMS' => [
		],
	],
	$component
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>